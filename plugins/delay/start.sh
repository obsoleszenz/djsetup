#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR
export GDK_BACKEND=x11;
pw-jack jalv.gtk3 https://distrho.kx.studio/plugins/cardinal#fx -l delay.state -n \"Delay\"
