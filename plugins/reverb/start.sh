#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR
export GDK_BACKEND=x11;
pw-jack jalv.gtk3 urn:dragonfly:room -l reverb.state/ -n \"Reverb\"
