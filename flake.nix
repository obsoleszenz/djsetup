{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    caja.url = "git+https://codeberg.org/obsoleszenz/caja?tag=v0.2.4";
    serial2midi.url = "git+https://codeberg.org/obsoleszenz/serial2midi?tag=v1.0.0";
    equis.url = "git+https://codeberg.org/obsoleszenz/equis?tag=fix_nix";
    librecdj.url = "git+https://codeberg.org/obsoleszenz/librecdj?tag=v0.2.0";
  };


  outputs = { self, nixpkgs, flake-utils, serial2midi, equis, librecdj, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        packageDependencies = with pkgs; [
            # misc. libraries
            openssl
            pkg-config

            # GUI libs
            libxkbcommon
            libGL
            fontconfig

            # wayland libraries
            wayland

            # x11 libraries
            xorg.libXcursor
            xorg.libXrandr
            xorg.libXi
            xorg.libX11

            # equis specific
            jack2
          
        ];
      in with pkgs; {
        devShells.default = mkShell rec {
          buildInputs = [
            serial2midi.packages.${system}.default
            equis.packages.${system}.default
            librecdj.packages.${system}.default
            
            dragonfly-reverb	
            cardinal
            jalv

            zellij
            tmux
            tmuxinator
            pipewire.jack
          ];
          LD_LIBRARY_PATH = "${lib.makeLibraryPath packageDependencies}";
        };
      });
}
