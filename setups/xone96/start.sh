#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

tmuxinator start -p $SCRIPT_DIR/tmuxinator-xone96.yml
